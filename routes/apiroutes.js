'use strict';

/**
 * APIへのルーティング設定
 */

var express = require('express');
var router = express.Router();

module.exports = function(app) {
  router.use('/store', require('../api/store')(app));
  router.use('/sample', require('../api/sample')(app));
  
  return router;
};
