'use strict';

/**
 * 各ページへのルーティング設定
 */

var express = require('express');
var router = express.Router();

module.exports = function (app) {

  router.use('/store', require('../app/store/controller'));
  router.get('/', require('../app/index'));

  return router;
}
