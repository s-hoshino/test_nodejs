'use strict';

/**
 * newmodel Model
 */

module.exports = function (app) {
  /**
   * GET list 全件取得
   */
  var list = function(){
    // 仮データ
    var data = {value: 'sample GET list'};

    // 全件取得処理

    return data;
  };

  /**
   * GET show 1件取得
   */
  var show = function(id){
    // 仮データ
    var data = {value: 'sample GET show'};

    // 1件取得処理

    return data;
  };

  /**
   * POST create 1件登録
   */
  var create = function(body){

    // 登録処理

    return true;
  };

  /**
   * PUT edit 1件編集
   */
  var edit = function(id, body){

    // 編集処理

    return true;
  };

  /**
   * DELETE destroy 1件削除
   */
  var destroy = function(id){

    // 削除処理

    return true;
  };
  
  return {
    list: list,
    show: show,
    create: create,
    edit: edit,
    del: destroy
  };
};
