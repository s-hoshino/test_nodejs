/**
 * store Model
 */
module.exports = function (app) {
  var path = require('path');
  var fs = require('fs');
  var storePath = path.join(__dirname, 'store.json');
  var store = JSON.parse(fs.readFileSync(storePath, 'utf8'));
  
  /*
   * store全件取得
   */
  var list = function(){
    return store;
  };

  /*
   * store1件取得
   */
  var show = function(id){
    if(store.length < Number(id)){
      throw new Error('OutOfLength');
    }
    var res = store[Number(id)-1];
    return res;
  };

  /*
   * store1件登録
   */
  var create = function(body){
    // bodyの確認
    var bName = body.name;
    var bData = body.data;
    console.log(bName);
    console.log(bData);

    if (bName === ''){
      bName = 'NoName';
    }

    // 登録データの作成
    var data = {
      'name': bName,
      'data': bData
    };

    // 登録処理
    store.push(data);
    fs.writeFile(storePath, JSON.stringify(store, null, '  '));

    return true;
  };

  /*
   * store1件編集
   */
  var edit = function(id, body){
    if(store.length < Number(id)){
      throw new Error('OutOfLength');
    }

    var bName = body.name;
    if (bName === ''){
      bName = 'NoName';
    }

    //中身の編集
    store[Number(id)-1]['name'] = bName;
    store[Number(id)-1]['data'] = body.data;

    //登録処理
    fs.writeFile(storePath, JSON.stringify(store, null, '  '));
    
    return true;
  };

  /*
   * store1件削除
   */
  var destroy = function(id){
    if(store.length < Number(id)){
      throw new Error('OutOfLength');
    }

    // 削除
    store.splice(Number(id)-1 , 1);

    //登録処理
    fs.writeFile(storePath, JSON.stringify(store, null, '  '));
    
    return true;
  };
  
  return {
    list: list,
    show: show,
    create: create,
    edit: edit,
    del: destroy
  };
};
