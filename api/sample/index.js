'use strict';

/**
 * newapi ルーティング設定
 */

var express = require('express');
var router = express.Router();

module.exports = function (app) {
  var controller = require('./controller')(app);
  
  router.get('/', controller.list);
  router.get('/:id(\\d+)', controller.show);
  
  router.post('/', controller.create);
  
  router.put('/:id(\\d+)', controller.edit);
  
  router.delete('/:id(\\d+)', controller.del);
  
  return router;
};