'use strict';

/**
 * API /api/newapi/ routing
 */

module.exports = function (app) {
  var logging = app.get('logger');
  var models = require('./models')(app);
  var views = require('./views')(app);
  
  var list = function(req, res, next) {
    logging.info('GET list');
    views.list(res, models.list());
  };

  var show = function(req, res, next) {
    logging.info('GET show');
    views.show(models.show(res, req.params.id));
  };
  
  var create = function(req, res, next) {
    logging.info('POST create');
    models.create(req.body);
    views.create(res, {status: 'POST success'});
  };
  
  var edit = function(req, res) {
    logging.info('PUT edit');
    models.edit(req.params.id, req.body);
    views.edit(res, { status: 'PUT success' });
  };
  
  var del = function(req, res) {
    logging.info('DELETE destroy');
    models.del(req.params.id);
    views.del(res, { status: 'DELETE success' });
  };
  
  return {
    list: list,
    show: show,
    create: create,
    edit: edit,
    del: del
  };
};
