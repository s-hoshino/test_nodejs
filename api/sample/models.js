'use strict';

/**
 * API /api/newapi/ models
 */

module.exports = function (app) {
  var models = require('../models/sample')(app);
  
  return {
    list: models.list,
    show: models.show,
    create: models.create,
    edit: models.edit,
    del: models.del
  };
};