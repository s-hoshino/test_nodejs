'use strict';

/**
 * store ルーティング設定
 */

var express = require('express');
var router = express.Router();

module.exports = function (app) {
  var controller = require('./controller')(app);
  
  router.get('/', controller.list, controller.list.error);
  router.get('/:id(\\d+)', controller.show, controller.show.error);
  
  router.post('/', controller.create, controller.create.error);
  
  router.put('/:id(\\d+)', controller.edit, controller.edit.error);
  
  router.delete('/:id(\\d+)', controller.del, controller.del.error);
  
  return router;
};