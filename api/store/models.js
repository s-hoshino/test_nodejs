'use strict';

/**
 * API /api/store/ models
 */

module.exports = function (app) {
  var models = require('../models/store')(app);
  
  return {
    list: models.list,
    show: models.show,
    create: models.create,
    edit: models.edit,
    del: models.del
  };
};