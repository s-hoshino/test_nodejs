'use strict';

/**
 * API /api/store/ controller
 */

module.exports = function (app) {
  var logging = app.get('logger');
  var models = require('./models')(app);
  var views = require('./views')(app);
  
  var list = function(req, res, next) {
    logging.info('GET list');
    views.list(res, models.list());
  };
  list.error = function(err, req, res, next) {
    // エラーが起こった場合の処理
    // エラー処理の具体的な中身は要検討
    logging.error(err);
    views.error(res);
  };

  var show = function(req, res, next) {
    logging.info('GET show');
    views.show(res, models.show(req.params.id));
  };
  show.error = function(err, req, res, next) {
    logging.error(err);
    views.error(res);
  };
  
  var create = function(req, res, next) {
    logging.info('POST create');
    models.create(req.body);
    views.create(res, {status: 'success'});
  };
  create.error = function(err, req, res, next) {
    logging.error(err);
    views.error(res);
  };
  
  var edit = function(req, res, next) {
    logging.info('PUT edit');
    models.edit(req.params.id, req.body);
    views.edit(res, { status: 'success' });
  };
  edit.error = function(err, req, res, next) {
    logging.error(err);
    views.error(res);
  };
  
  var del = function(req, res, next) {
    logging.info('DELETE destroy');
    models.del(req.params.id);
    views.del(res, { status: 'success' });
  };
  del.error = function(err, req, res, next) {
    logging.error(err);
    views.error(res);
  };
  
  return {
    list: list,
    show: show,
    create: create,
    edit: edit,
    del: del
  };
};
