'use strict';

/**
 * API /api/sample/ views
 */

module.exports = function(app){
  var logger = app.get('logger');
  
  var list = function(res, params){
    res.status(200).json(params);
  };
  
  var show= function(res, params){
    res.status(200).json(params);
  };
  
  var create= function(res, params){
    res.status(201).json(params);
  };
  
  var edit= function(res, params){
    res.status(202).json(params);
  };
  
  var del= function(res, params){
    res.status(202).json(params);
  };
  
  var error = function(res){
    res.status(500).json({ status: 'error'});
  };
  
  return {
    list: list,
    show: show,
    create: create,
    edit: edit,
    del: del,
    error: error
  };
};