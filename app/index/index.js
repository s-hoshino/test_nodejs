var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Sunvisor IoT test', title2: 'RESTful API Sample' });
});

module.exports = router;
