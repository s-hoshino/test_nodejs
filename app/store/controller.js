/**
 * /store ルーティング
 */

var express = require('express');
var router = express.Router();
var RestClient = require('node-rest-client').Client;
var apiClient = new RestClient();
var config = require('config');
var apiRoot = config.getApiRoot();

/*
 *  data list page.
 *  store一覧ページ
 */
router.get('/', function(req, res, next) {
  var apiUrl = 'api/store/';
  var requestUrl = apiRoot + apiUrl;

  console.log('API URL: ' + requestUrl);

  apiClient.get(requestUrl, function (data, response) {
    // APIからのレスポンス確認
//    console.log(data);

    res.render('store/list', {data: data});
  });
});

/*
 *  data regist page.
 *  store登録ページ
 */
router.get('/regist/', function(req, res, next) {
  // 登録用ページの表示
  res.render('store/regist');
});

/*
 *  data create.
 *  store登録アクション
 */
router.post('/regist/', function(req, res, next) {
  var apiUrl = 'api/store/';
  var requestUrl = apiRoot + apiUrl;

  console.log('API URL: ' + requestUrl);
  console.log(req.body);

  // APIへ投げるrequest bodyの用意
  var args = {
    data: req.body,
    headers: { "Content-Type": "application/json" }
  };

  // APIへリクエスト
  apiClient.post(requestUrl, args, function (data, response) {
    // APIからのレスポンス確認
    console.log(data);

    // 一覧へリダイレクト
    res.redirect(302, '/store');
  });
});

/*
 *  data show page.
 *  store詳細ページ
 */
router.get('/:id', function(req, res, next) {
  var apiUrl = 'api/store/' + req.params.id;
  var requestUrl = apiRoot + apiUrl;

  console.log('API URL: ' + requestUrl);

  apiClient.get(requestUrl, function (data, response) {
    // APIからのレスポンス確認
    console.log(data);

    res.render('store/show', {storeId: req.params.id, name: data['name'], data: data['data']});
  });
});

/*
 *  store編集ページ
 */
router.get('/:id/edit', function(req, res, next){
  var apiUrl = 'api/store/' + req.params.id;
  var requestUrl = apiRoot + apiUrl;

  console.log('API URL: ' + requestUrl);

  apiClient.get(requestUrl, function (data, response) {
    // APIからのレスポンス確認
    console.log(data);

    res.render('store/edit', {storeId: req.params.id, name: data['name'], data: data['data']});
  });
});

/*
 *  store編集アクション
 */
router.post('/:id/edit', function(req, res, next){
  var apiUrl = 'api/store/' + req.params.id;
  var requestUrl = apiRoot + apiUrl;

  console.log('API URL: ' + requestUrl);

  //APIへ投げるrequest bodyの用意
  var args = {
    data: req.body,
    headers: { "Content-Type": "application/json" }
  };

  apiClient.put(requestUrl, args, function (data, response) {
    // APIからのレスポンス確認
    console.log(data);

    // 詳細表示へリダイレクト
    res.redirect(302, '/store/' + req.params.id);
  });
});

/*
 *  store削除アクション
 */
router.get('/:id/delete', function(req, res, next){
  var apiUrl = 'api/store/' + req.params.id;
  var requestUrl = apiRoot + apiUrl;

  console.log('API URL: ' + requestUrl);

  apiClient.delete(requestUrl, function (data, response) {
    // APIからのレスポンス確認
    console.log(data);

    // 詳細表示へリダイレクト
    res.redirect(302, '/store');
  });
});

module.exports = router;