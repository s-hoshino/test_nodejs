/**
 * アプリケーション設定
 */

var ENV_CONFIG = {
  development: {
    apiRoot: {
      local: 'http://localhost:3000/',
      aws: 'http://sunvisoriot-devenv.us-west-2.elasticbeanstalk.com/',
    },
  },
  production: {
    apiRoot: {
      local: 'http://localhost:3000/',
      aws: 'http://sunvisoriot-front.us-west-2.elasticbeanstalk.com/',
    },
  }
};

exports.getEnvConf = function(){
  var env = process.env.NODE_ENV || 'production';
  console.log('env_conf: ' + env);
  console.log(ENV_CONFIG[env]);
  return ENV_CONFIG[env];
};

exports.getApiRoot = function(){
  var env = process.env.NODE_ENV || 'production';
  var location = process.env.NODE_LOCATION || 'aws';
  return ENV_CONFIG[env].apiRoot[location];
};


