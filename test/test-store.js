/**
 * test sample
 */
var assert = require('assert');
var should = require('should');

var sinon = require('sinon');
var request = require('supertest');
var cuckoo = require('cuckoo');
var proxyquire = require('proxyquire');

var express = require('express');
var app = express();

var storeModels = require('../api/store/models')(app);

describe('store', function () {
  before(function(done) {
    // テストが始まる前の処理
    done();
  });

  after(function(done) {
    // テストが終わった後の処理
    done();
  });
  
  describe('models' ,function(){
    // assert
    describe('list', function(){
      it('test1 assert', function(){
        assert.equal(storeModels.list().length, 4);
      });
    });
    
    // should
    describe('show', function(){
      it('test2 should', function(){
        var response = storeModels.show('1');
        response.should.have.property('name');
      });
    });
  });
  
  describe('controller', function(){
    /** 
     * @todo: cuckoo, supertest, proxyquire, (sinon)の試し書き
     */
    
    var req, res, next;
    
    /**
     * 同階層のdescribe内it実行前の処理
     */
    beforeEach(function(done) {
      // req, res, nextのfake
      console.log('\t[beforeEach] Init Fake req, res, next');
      req = {};
      res = {};
      next = {};
      done();
    });
    
    /**
     * Test: api.store.controller
     * (テストモジュールの試し書き用)
     */
    describe('list', function(){
      // loggerのfakeをappにset
      var fakeLogger = {
        info: function(message){
          return message;
        },
        warn: function(message){
          return message;
        },
        error: function(message){
          return message;
        },
        fatal: function(message){
          return message;
        }
      };
      app.set('logger', fakeLogger);
      
      /**
       * proxyquireの試用
       * (テストスタブを用いたテスト)
       */
      it('test proxyquire', function(){
        console.log('\tfakeLogger: ' + fakeLogger.info('test'));
        
        // Fake res.json
        res.json = function (list){
          list.should.have.length(1);
          list[0].should.have.property('name');
          list[0].should.have.property('data');
          return list;
        };
        
        // modelsのstubを用意
        var modelsStub = function (app) {
          var list = function() {
            return [{
              'name': 'testname',
              'data': 'testdata'
            }];
          };
          return {
            list: list
          };
        };
        
        var stubs = {
          './models': modelsStub
        };
        
        var controller = proxyquire('../api/store/controller', stubs)(app);
        controller.list(req, res, next);
      });
      
      /**
       * supertestの試用
       * (ルーティング，APIのテスト)
       */
      it('test supertest', function(){
        var app = require('../server');
        var body;
        request(app)
        .get('/api/store/')
        .end(function(err, res){
          body = res.body;
          body[0].should.have.property('name');
          body[0].should.have.property('data');
          console.log('\t' + 'done');
          done();
        });
      });
      
      /**
       * sinon.jsの試用
       */
      it('test sinon', function(){
        var spy = sinon.spy();
      });
    });
  });
});
















